import cv2


def detect_stop_signs(image):

    sign_cascade = cv2.CascadeClassifier("stop.xml")
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    signs = sign_cascade.detectMultiScale(gray, 1.1, 4)
    # print(signs)
    for (x, y, w, h) in signs:
        cv2.putText(image, "STOP SIGN", ((x + int(0.2 * w)), y + 100), cv2.FONT_ITALIC, 1, (0, 0, 255), 2)
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)
    detected_image = image
    #cv2.imshow("DETECTION LIVE", image)
    return detected_image,signs




if __name__ == "__main__":
    camera = cv2.VideoCapture(0)
    while 1:
        success, image = camera.read()

        sign_cascade = cv2.CascadeClassifier("stop.xml")
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        signs = sign_cascade.detectMultiScale(gray, 1.1, 4)
        print(signs)
        for (x, y, w, h) in signs:
            cv2.putText(image, "STOP SIGN", ((x + int(0.2 * w)), y - 30), cv2.FONT_ITALIC, 1, (0, 0, 255), 4)
            cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)

        cv2.imshow("DETECTION LIVE", image)
        if cv2.waitKey(2) & 0xFF == ord('Q'):
            break
    camera.release()
    cv2.destroyAllWindows()




