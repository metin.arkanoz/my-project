import WebCam
import LaneDetection
from Motor_Script import motor
import socket
import cv2
import pickle
import struct


def server_starter():
    # Socket Create
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host_name = socket.gethostname()
    host_ip = socket.gethostbyname(host_name)
    print('HOST IP:', host_ip)
    port = 9999
    socket_address = (host_ip, port)

    # Socket Bind
    server_socket.bind(socket_address)


def server_run(imgSTACKED):
    # Socket Listening
    server_socket.listen(5)
    print("LISTENING AT:", socket_address)

    # Socket Accepting
    while True:
        client_socket, addr = server_socket.accept()
        print('GOT CONNECTION FROM:', addr)
        if client_socket:
            while (True):
                frame = imgSTACKED
                a = pickle.dumps(frame)
                message = struct.pack("Q", len(a)) + a
                client_socket.sendall(message)

                #cv2.imshow('TRANSMITTING VIDEO', frame)
                key = cv2.waitKey(1) & 0xFF
                if key == ord('q'):
                    client_socket.close()



def main():
    #motor should be defined !
    img = Webcam.run_cam(display=False)
    curveVal , imgSTACKED,coordinates = LaneDetection.getLaneCurve(img, 1)
    server_run(imgSTACKED)
    sen = 1.3  # SENSITIVITY
    maxVAl = 0.3  # MAX SPEED

    if curveVal > maxVAl:
        curveVal = maxVAl

    if curveVal < -maxVAl:
        curveVal = -maxVAl

    # print(curveVal)
    if curveVal > 0:
        sen = 1.7
        if curveVal < 0.05: curveVal = 0
    else:
        if curveVal > -0.08: curveVal = 0
    # Still we have a problem controlling motor according to detection of stop signs
    if len(coordinates)>0:

        motor.stop(1)
    motor.move(0.20, -curveVal * sen, 0.05)
    # cv2.waitKey(1)


if __name__ == '__main__':
    server_starter()
    while True:
        main()

