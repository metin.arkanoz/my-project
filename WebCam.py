import cv2

cam= cv2.VideoCapture(0)


def run_cam(size=[480,240],display=False):
    success,image = cam.read()
    image = cv2.resize(image,size[0],size[1])
    if display:
        cv2.imshow('IMG',image)
    return  image


if __name__ == '__main__':
    while True:
        run_cam(display=True)

